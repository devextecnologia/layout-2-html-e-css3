const gulp      = require("gulp")
const webp      = require('gulp-webp')
const babel     = require('gulp-babel')
const uglify    = require('gulp-uglify')
const uglifycss = require('gulp-uglifycss')
const rename    = require('gulp-rename')
const htmlmin   = require('gulp-htmlmin')
const concat    = require('gulp-concat')

gulp.task('images', () => {
    gulp.src('./src/images/*.png')
    .pipe(webp())
    .pipe(gulp.dest('./public/images'))
})

gulp.task('css', () => {
  gulp.src('./src/css/*.css')
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(rename({
      basename: "main",
      suffix: ".min"
    }))
    .pipe(gulp.dest('./public/css'))
})

gulp.task('scripts', () => {
  gulp.src('./src/js/*.js')
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(uglify())
    .pipe(rename({
      suffix: ".min"
    }))
    .pipe(gulp.dest('./public/js'))
})

gulp.task('html', () => {
  gulp.src('./src/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./'));
})

gulp.task('watch', () => {
  gulp.watch('./src/*.html', gulp.series('html'))
  gulp.watch('./src/css/*.css', gulp.series('css'))
  gulp.watch('./src/js/*.js', gulp.series('scripts'))
  gulp.watch('./src/images/*.png', gulp.series('images'))
})

gulp.task('default', gulp.series('watch'))