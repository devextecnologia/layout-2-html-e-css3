$(function() {

    $("form[name='economy-calc']").validate({
        rules: {
            "last-account-value": "required",
            name: "required",
            email: {
                required: true,
				email: true
            },
            phone: "required"
        },
        messages: {
            "last-account-value": "Informe o valor correto!",
            name: "Informe seu nome!",
            email: "Informe seu email!",
            phone: "Informe seu telefone!"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $("form[name='request-proposal']").validate({
        rules: {
            name: "required",
            business: "required",
            email: {
                required: true,
				email: true
            },
            cargo: "required",
            phone: "required",
            "last-account": "required"
        },
        messages: {
            name: "Informe seu nome!",
            business: "Informe sua empresa!",
            email: "Informe seu email!",
            cargo: "Informe seu cargo!",
            phone: "Informe seu telefone!",
            cnpj: "Informe seu cnpj!",
            "last-account": "Informe o valor correto!"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $(document).on('click', '.navbar-nav a[href^="#"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top}, 500);
    });

    $(document).on('click', 'span.scroll-top-arrow', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $('span.scroll-top-arrow').offset().top}, 500);
    });

    $(document).ready(function(){
        $("form[name='economy-calc'] #phone").inputmask("(99) 9999-9999");
        $("form[name='economy-calc'] #last-account-value").inputmask();
        $("form[name='request-proposal'] #cnpj").inputmask("99.999.999/9999-99")
        $("form[name='request-proposal'] #phone").inputmask("(99) 9999-9999");
        
    });

    $(document).ready(function(){
        const obj = [
            {
                "id": 1,
                "question": "1 - Existe algum pré requisito para contratar os serviços da Eva Energia?",
                "answer": "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
            },
            {
                "id": 2,
                "question": "2- Quais são as áreas que vocês atuam?",
                "answer": "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
            },
            {
                "id": 3,
                "question": "3- É preciso fazer alguma instalação especial?",
                "answer": "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
            },
            {
                "id": 4,
                "question": "4- Existe algum investimento inicial na assinatura do contrato?",
                "answer": "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
            },
            {
                "id": 5,
                "question": "5- Qual o tempo mínimo de contrato?",
                "answer": "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
            },
            {
                "id": 6,
                "question": "6- Quem irá prover minha energia?",
                "answer": "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
            },
        ];
        for(let i = 0; i < obj.length; i++) {
            let item = obj[i];
            $("#questions").append('<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading'+ item.id +'"><h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+ item.id +'" aria-expanded="true" aria-controls="collapse'+ item.id +'">'+ item.question +'</a></h4></div><div id="collapse'+ item.id +'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+ item.id +'"><div class="panel-body">'+ item.answer +'</div></div></div>');
        };
    });
    
    $(document).ready(function(){
        $(document).scroll(function(){

            const scrollHeight =  $(document).scrollTop();
            const countItems = $("#vantagens .animation").length;

            const animations = {
                1: 'animate__animated animate__zoomInLeft', 
                2: 'animate__animated animate__zoomInRight',
                3: 'animate__animated animate__zoomInLeft',
                4: 'animate__animated animate__zoomInRight',
                5: 'animate__animated animate__zoomInLeft',
                6: 'animate__animated animate__zoomInRight',
            };

            for(let i = 1; i <= countItems; i++){
                if(scrollHeight > 1870){
                    $("#animation" + i).addClass(animations[i]);
                }
            }

            if(scrollHeight > 1390){
                $("form[name='economy-calc']").addClass('animate__animated animate__fadeInLeft');
            }

            if(scrollHeight > 2857){
                $("form[name='request-proposal']").addClass('animate__animated animate__fadeInRight');
            }   

      });
    });

    $(window).on('load', function() {
        $('#modal').modal('show');
    });

});